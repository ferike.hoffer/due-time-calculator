<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Commands;

use App\DueDateCalculator\Contracts\IDueDateCalculatorService;
use App\DueDateCalculator\Enums\DueDateCalculatorEnum;
use DateTime;
use DateTimeZone;
use Illuminate\Console\Command;
use Exception;

class DueDateCalculatorCommand extends Command
{
    public function __construct(private readonly IDueDateCalculatorService $dueDateCalculatorService)
    {
        parent::__construct();
    }

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'due-date-calculator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle(): void
    {
        $timeZone = new DateTimeZone(DueDateCalculatorEnum::TIMEZONE_BUDAPEST);
        $now = (new DateTime('now', $timeZone))->format('Y-m-d H:i');
        $transitHours = 10;

        echo $this->dueDateCalculatorService->calculateDueDate(
            $now,
            $transitHours
        );
    }
}
