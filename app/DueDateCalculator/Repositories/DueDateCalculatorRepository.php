<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Repositories;

use App\DueDateCalculator\Contracts\IDueDateCalculatorRepository;
class DueDateCalculatorRepository implements IDueDateCalculatorRepository
{
    /**
     * For DB operations
     * @return void
     */
    public function exampleFunction(): void
    {

    }
}
