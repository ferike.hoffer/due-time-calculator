<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Enums;
enum DueDateCalculatorEnum
{
    public const START_HOUR = 9;
    public const END_HOUR = 17;
    public const WEEKEND_DAYS = ['Sat', 'Sun'];
    public const TIMEZONE_BUDAPEST = 'Europe/Budapest';
    public const BASE_FORMAT = 'Y-m-d H:i';
    public const INVALID_FORMAT_MESSAGE = 'Invalid Date the correct format is : ' . self::BASE_FORMAT;
}
