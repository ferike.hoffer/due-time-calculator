<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Contracts;

use App\DueDateCalculator\Enums\DueDateCalculatorEnum;
use DateTime;
use Exception;
interface IDueDateCalculatorService
{
    /**
     * @throws Exception
     */
    public function calculateDueDate(string $startDate, int $transitHours): string;
}
