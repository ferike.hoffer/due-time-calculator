<?php

declare(strict_types=1);

namespace DueDateCalculator\Services;

use App\DueDateCalculator\Services\DueDateCalculatorService;
use App\DueDateCalculator\Enums\DueDateCalculatorEnum;
use PHPUnit\Framework\TestCase;
use DateTime;
use Exception;

class DueDateCalculatorServiceTest extends TestCase
{
    private DueDateCalculatorService $dueDateCalculatorService;

    protected function setUp(): void
    {
        $this->dueDateCalculatorService = new DueDateCalculatorService();
    }

    /**
     * @throws Exception
     */
    public function testCalculateDueDateWithValidDate(): void
    {
        $startDate = '2023-08-11 18:30';
        $transitHours = 40;

        $expectedDueDate = '2023-08-21 09:30';
        $actualDueDate = $this->dueDateCalculatorService->calculateDueDate($startDate, $transitHours);

        $this->assertEquals($expectedDueDate, $actualDueDate);
    }

    /**
     * @dataProvider setToNextValidDateProvider
     */
    public function testSetToNextValidDate($reportDate, $additionalMinutes, $expectedDate): void
    {
        $result = $this->dueDateCalculatorService->setToNextValidDate($reportDate, $additionalMinutes);
        $this->assertEquals($expectedDate, $result);
    }

    /**
     * @dataProvider validDateProvider
     */
    public function testIsValidDateWithValidDates(string $date, string $format): void
    {
        $this->assertTrue($this->dueDateCalculatorService->isValidDate($date, $format));
    }

    /**
     * @dataProvider invalidDateProvider
     */
    public function testIsValidDateWithInvalidDates(string $date, string $format): void
    {
        $this->assertFalse($this->dueDateCalculatorService->isValidDate($date, $format));
    }

    public static function setToNextValidDateProvider(): array
    {
        return [
            [new DateTime('2023-08-10 04:00'), 30, new DateTime('2023-08-10 09:30')],
            [new DateTime('2023-08-10 18:00'), 30, new DateTime('2023-08-11 09:30:00')],
            [new DateTime('2023-08-11 18:00'), 30, new DateTime('2023-08-14 09:30:00')],
        ];
    }

    public static function validDateProvider(): array
    {
        return [
            ['2023-08-10 11:31', DueDateCalculatorEnum::BASE_FORMAT],
            ['2023-08-11 21:21', DueDateCalculatorEnum::BASE_FORMAT]
        ];
    }

    public static function invalidDateProvider(): array
    {
        return [
            ['2022-13-01', 'Y-m-d'],
            ['01/32/2022', 'm/d/Y'],
            ['2022-01-01 25:00:00', 'Y-m-d H:i:s'],
            ['2022-01-01', 'd/m/Y'],
        ];
    }
}
