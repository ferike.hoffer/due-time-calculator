<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Services;

use App\DueDateCalculator\Contracts\IDueDateCalculatorService;
use App\DueDateCalculator\Enums\DueDateCalculatorEnum;
use DateInterval;
use DateTime;

class DueDateCalculatorService implements IDueDateCalculatorService
{
    public function calculateDueDate(string $startDate, int $transitHours): string
    {
        if (!$this->isValidDate($startDate)) {
            return DueDateCalculatorEnum::INVALID_FORMAT_MESSAGE;
        }

        $reportDate = new DateTime($startDate);
        $additionalMinutes = (int)$reportDate->format('i');
        $expectedTime = $this->setToNextValidDate($reportDate, $additionalMinutes);
        while ($transitHours >= 1) {
            $expectedTime->add(new DateInterval('PT1H'));
            $expectedTime = $this->setToNextValidDate($expectedTime, $additionalMinutes);
            $transitHours--;
        }

        return $expectedTime->format(DueDateCalculatorEnum::BASE_FORMAT);
    }

    public function setToNextValidDate(DateTime $reportDate, int $additionalMinutes): DateTime
    {
        $dueDate = (clone $reportDate);

        if ($dueDate->format('G') < DueDateCalculatorEnum::START_HOUR) {
            $dueDate->setTime(DueDateCalculatorEnum::START_HOUR, $additionalMinutes);
        }

        if ($dueDate->format('G') >= DueDateCalculatorEnum::END_HOUR) {
            $dueDate->add(new DateInterval('PT15H'));
            $dueDate->setTime(DueDateCalculatorEnum::START_HOUR, $additionalMinutes);
        }

        if (in_array($dueDate->format('D'), DueDateCalculatorEnum::WEEKEND_DAYS)) {
            $dueDate = $dueDate->modify(
                'next monday ' . DueDateCalculatorEnum::START_HOUR . ':' . $additionalMinutes
            );
        }

        return $dueDate;
    }

    public function isValidDate(string $date, string $format = DueDateCalculatorEnum::BASE_FORMAT): bool
    {
        $dateTime = DateTime::createFromFormat($format, $date);

        return $dateTime && $dateTime->format($format) === $date;
    }
}
