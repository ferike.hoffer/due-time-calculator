<?php

declare(strict_types=1);

namespace App\DueDateCalculator\Providers;

use Illuminate\Support\ServiceProvider;
use App\DueDateCalculator\Contracts\IDueDateCalculatorService;
use App\DueDateCalculator\Services\DueDateCalculatorService;
use App\DueDateCalculator\Contracts\IDueDateCalculatorRepository;
use App\DueDateCalculator\Repositories\DueDateCalculatorRepository;

class DueDateCalculatorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(IDueDateCalculatorService::class, DueDateCalculatorService::class);
        $this->app->bind(IDueDateCalculatorRepository::class, DueDateCalculatorRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
